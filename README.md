# Walk through a full Devops setup from scratch

## Requirements:
 - Setup orchestration of Cloud/On-prem DC (Dev/Staging/Prod/Disaster Recovery)
 - Automate as much as possible
 - Full CICD setup
 - Backend application (Python/Django RestApi) connected to PostgreSQL
 - Frontend application showing information from backend via API
 - Find an efficient solution to distribute the massive network traffic across the cluster
 - Auto setup/management of HTTPS certificates
 - Ensure Load balancing, High-Availability, high resiliency + high throughput of all  services
 - all well documented and available on "single point of truth" (Gitlab/Github)
 - simple adoption from developers


## Solution:
- Abstract away orchestration with Terraform:
	- Terraform will setup:
	- IAM/Service accounts
	- Kubernetes
	- Multiple nodes with Autoscaling will provide resiliency and HA
	- Databases
	- Managed, backed up and in HA mode
	- Storage (Persistant storage/Buckets)
	- backed up + HA
- Helm will configure Kubernetes resources:
	- Letsencrypt certificate auto creation/renewal
	- certificate will be automatically obtained for each new service added
	- HA ensured by Kubernetes scheduling replica sets
	- Konghq Api Gateway
	- enhance Authorization/Authentication, resiliency, security and API management
	- in HA with running on managed PosgreSQL
	- with Public Static IP
- CI/CD setup:
	- Gitlab CI/CD pipeline will:
	- run automatically upon commit to Gitlab
	- different pipeline will run depending on branch (Master/Dev/Fix/Other)
	- build stage: build Docker images + push to gitlab registry
	- test stage: run unit tests on image
	- staging stage: push to Staging environment + run Unit/Integration tests
	- production stage: push to Production environment
	- HA ensured by Gitlab runners setup
	- deploy all given resources incl.:
	- Backend/Frontend applications/services
	- Terraform deployments
	- Helm deployments
- Applications/services:
	- HA ensured by Kubernetes scheduling replica sets + autoscaling
	- All Apps are fully dynamic >> all env. vars are dynamically asigned to Pod upon startup via K8s secrets
	- All backend applications use side-car (proxy) pattern to connect to Gcloud Database
	- Backend applications are all created from a single Template (Python-Cookiecutter)


## Considerations:
 - Terraform stats file includes sensitive data
 - Enahnced DNS could be better for cross application communication
 - Enhancing security:
 	- Manage/Secure communication within K8s cluster (POC Consul/ISTIO)
 		- Limit Applications network within POD to only talk via localhost
 - How update a concrete artifact inside the web server layer
 	- If commit to gitlab and run cicd is not an option:
 		- Best option - Ansible to ssh into PODs to make changes
 		- Alternatively - run Puppet Enterprise with master, setup client on each Apps 
 		image so they register as PODs starts and Puppet can manage them dynamically
 - K8s secrets may be better/safer to store in Vault 

##  Future enhancements:
 - Auto push Terraform stats files to safe location (e.g. Consul)
 - Kubernetes DNS (POC Consul)
 - HA ensured by Kubernetes scheduling replica sets
 - POC Istio/Consul to secure network within cluster
 - POC Vault for storing secrets


## Start setup here:
[full gcloud project repo](https://gitlab.com/daniel-svoboda/devops/google-cloud-specific/setup-full-cloud-guide)
